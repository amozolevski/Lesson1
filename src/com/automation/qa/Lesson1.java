package com.automation.qa;

import java.util.*;

public class Lesson1 {
    private static int randomValue;

    public static void main(String[] args) {

        int myValue = 7;

        if (myValue == randomValue){
            winMessage(myValue);
        } else {
            while (myValue != randomValue) {

                makeRandomValue(10);

                if (myValue == randomValue) {
                    winMessage(myValue);
                } else {
                    System.out.println(randomValue);
                }
            }
        }
    }

    private static void makeRandomValue(int amount){
        randomValue = new Random().nextInt(amount);
    }

    private static void winMessage(int value){ System.out.println("Congratulations, the number was: " + value); }
}
